[[ -s "/Users/hammerdr/.rvm/scripts/rvm" ]] && source "/Users/hammerdr/.rvm/scripts/rvm"  # This loads RVM into a shell session.

alias mongo_start="launchctl load -w ~/Library/LaunchAgents/org.mongodb.mongod.plist"
alias mongo_stop="launchctl unload -w ~/Library/LaunchAgents/org.mongodb.mongod.plist"
alias ls="ls -laG"
alias git=hub
